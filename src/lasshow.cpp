/*
    lastools
    Copyright (C) 2015 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/dazzler/db/DatabaseFile.hpp>
#include <libmaus2/parallel/NumCpus.hpp>
#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/lcs/NP.hpp>
#include <libmaus2/lcs/AlignmentPrint.hpp>
#include <libmaus2/dazzler/align/OverlapIndexer.hpp>
#include <libmaus2/dazzler/align/SimpleOverlapParser.hpp>
#include <libmaus2/dazzler/align/OverlapDataInterface.hpp>

#include <config.h>

std::string getUsage(libmaus2::util::ArgParser const & arg)
{
	std::ostringstream ostr;

	ostr << "usage: " << arg.progname << " [-a -t] <in_a.db> <in_b.db> <in.las>" << std::endl;
	ostr << "\n";
	ostr << "parameters:\n";
	ostr << " -a: show alignment" << std::endl;
	ostr << " -t: show trace data" << std::endl;

	return ostr.str();
}

int64_t getDefaultTSpace()
{
	return libmaus2::dazzler::align::AlignmentFile::getMinimumNonSmallTspace();
}

static char mapFunction(int const c)
{
	return c;
}

int lasshow(libmaus2::util::ArgParser const & arg)
{
	bool const showalignment = arg.uniqueArgPresent("a");
	bool const showtrace = arg.uniqueArgPresent("t");
	int64_t const vaid = arg.uniqueArgPresent("aid") ? arg.getUnsignedNumericArg<uint64_t>("aid") : -1;
	int64_t const vbid = arg.uniqueArgPresent("bid") ? arg.getUnsignedNumericArg<uint64_t>("bid") : -1;
	int64_t const cols = arg.uniqueArgPresent("c") ? arg.getUnsignedNumericArg<uint64_t>("c") : 80;

	std::string const db0name = arg[0];
	std::string const db1name = arg[1];

	libmaus2::dazzler::db::DatabaseFile::unique_ptr_type uDB0;
	libmaus2::dazzler::db::DatabaseFile * pDB0 = 0;
	libmaus2::dazzler::db::DatabaseFile::unique_ptr_type uDB1;
	libmaus2::dazzler::db::DatabaseFile * pDB1 = 0;

	{
		libmaus2::dazzler::db::DatabaseFile::unique_ptr_type tDB0(new libmaus2::dazzler::db::DatabaseFile(db0name));
		uDB0 = std::move(tDB0);
		pDB0 = uDB0.get();
	}

	if ( db1name == db0name )
	{
		pDB1 = uDB0.get();
	}
	else
	{
		libmaus2::dazzler::db::DatabaseFile::unique_ptr_type tDB1(new libmaus2::dazzler::db::DatabaseFile(db1name));
		uDB1 = std::move(tDB1);
		pDB1 = uDB1.get();
	}

	libmaus2::dazzler::db::DatabaseFile & DB0 = *pDB0;
	DB0.computeTrimVector();
	std::vector<uint64_t> RL0;
	DB0.getAllReadLengths(RL0);

	libmaus2::dazzler::db::DatabaseFile & DB1 = *pDB1;
	DB1.computeTrimVector();
	std::vector<uint64_t> RL1;
	DB1.getAllReadLengths(RL1);

	libmaus2::lcs::NP NP;
	libmaus2::lcs::AlignmentTraceContainer ATC;

	std::vector<std::string> Vin;
	for ( uint64_t i = 2; i < arg.size(); ++i )
		Vin.push_back(arg[i]);

	libmaus2::dazzler::align::SimpleOverlapParserConcat SOPC(Vin,64*1024 /* bs */);
	libmaus2::dazzler::align::SimpleOverlapParserConcatGet SOPCG(SOPC);
	int64_t const tspace = SOPC.tspace;
	std::pair<uint8_t const *,uint8_t const *> P;
	std::string sa;
	int64_t aid = -1;
	std::string said;
	std::string sb;
	std::string sbr;
	int64_t bid = -1;
	std::string sbid;
	libmaus2::autoarray::AutoArray<std::pair<uint16_t,uint16_t> > A16;

	while ( SOPCG.getNext(P) )
	{
		libmaus2::dazzler::align::OverlapDataInterface ODI(P.first);

		if (
			(vaid < 0 || vaid == ODI.aread())
			&&
			(vbid < 0 || vbid == ODI.bread())
		)
		{
			std::cout
				<< ODI.aread()
				<< "("
				<< DB0.getBlockForIdTrimmed(ODI.aread())
				<< ")"
				<< "[" << ODI.abpos() << "," << ODI.aepos() << ")/" << RL0[ODI.aread()]
				<< " "
				<< ODI.bread()
				<< "("
				<< DB1.getBlockForIdTrimmed(ODI.bread())
				<< ")"
				<< (ODI.isInverse()?'c':'n')
				<< "[" << ODI.bbpos() << "," << ODI.bepos() << ")/" << RL1[ODI.bread()]
				<< " diffs " << ODI.diffs()
				<< " e " << ODI.getErrorRate()
				;

			if ( ODI.getTrueFlag() )
			{
				std::cout << " " << "true";
			}
			if ( ODI.getHaploFlag() )
			{
				std::cout << " " << "haplo";
			}

			if ( showalignment )
			{
				if ( ODI.aread() != aid )
				{
					aid = ODI.aread();
					sa = DB0[aid];
					said = libmaus2::util::NumberSerialisation::formatNumber(aid,0);

					//std::cerr << "[V] aid=" << aid << " length=" << a.size() << std::endl;
				}
				if ( ODI.bread() != bid )
				{
					bid = ODI.bread();
					sb = DB1[bid];
					sbid = libmaus2::util::NumberSerialisation::formatNumber(bid,0);
					sbr = libmaus2::fastx::reverseComplementUnmapped(sb);
				}

				uint8_t const * aptr = reinterpret_cast<uint8_t const *>(sa.c_str());
				uint8_t const * bptr =
					ODI.isInverse()
					?
					reinterpret_cast<uint8_t const *>(sbr.c_str())
					:
					reinterpret_cast<uint8_t const *>( sb.c_str());

				ODI.computeTrace(A16,tspace,aptr,bptr,ATC,NP);

				std::cout << " " << ATC.getAlignmentStatistics() << "\n";
				std::cout.put('\n');

				libmaus2::lcs::AlignmentPrint::printAlignmentLines(
					std::cout,
					aptr + ODI.abpos(), ODI.aepos()-ODI.abpos(),
					bptr + ODI.bbpos(), ODI.bepos()-ODI.bbpos(),
					cols,
					ATC.ta,
					ATC.te,
					mapFunction,
					ODI.abpos(),
					ODI.bbpos(),
					said,
					sbid
				);
			}
			else
			{
				std::cout.put('\n');
			}

			if ( showtrace )
			{
				uint64_t const nt = ODI.decodeTraceVector(A16,tspace);
				for ( uint64_t i = 0; i < nt; ++i )
					std::cout << "path[" << i << "]=(" << A16[i].first << "," << A16[i].second << ")" << "\n";
			}

		}
	}

	return EXIT_SUCCESS;
}

/**
 * sort a set of LAS/OVL files and merge the sorted files to a single output file
 **/

int main(int argc, char *argv[])
{
	try
	{
		libmaus2::util::ArgInfo const arginfo(argc,argv);
		libmaus2::util::ArgParser const arg(argc,argv);

		if ( arg.argPresent("h") || arg.argPresent("help") )
		{
			std::cerr << getUsage(arg);
			return EXIT_SUCCESS;
		}
		else if ( arg.argPresent("version") )
		{
			std::cerr << "This is " << PACKAGE_NAME << " version " << PACKAGE_VERSION << std::endl;
			return EXIT_SUCCESS;
		}
		else if ( arg.size() < 3 )
		{
			std::cerr << getUsage(arg);
			return EXIT_FAILURE;
		}

		return lasshow(arg);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
