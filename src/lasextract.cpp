/*
    Copyright (C) 2018 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/util/GetFileSize.hpp>
#include <libmaus2/dazzler/db/DatabaseFile.hpp>
#include <libmaus2/dazzler/align/SimpleOverlapParser.hpp>
#include <libmaus2/dazzler/align/OverlapDataInterface.hpp>
#include <libmaus2/dazzler/align/AlignmentWriter.hpp>

int lasextract(libmaus2::util::ArgParser const & arg)
{
	std::string const outlas    = arg[0];
	std::string const outfasta1 = arg[1];
	std::string const outfasta2 = arg[2];
	std::string const indb1 = arg[3];
	std::string const indb2 = arg[4];
	std::string const inlas = arg[5];

	if ( libmaus2::util::GetFileSize::fileExists(outlas) )
	{
		std::cerr << "[E] file " << outlas << " already exists" << std::endl;
		return EXIT_FAILURE;
	}
	if ( libmaus2::util::GetFileSize::fileExists(outfasta1) )
	{
		std::cerr << "[E] file " << outfasta1 << " already exists" << std::endl;
		return EXIT_FAILURE;
	}
	if ( libmaus2::util::GetFileSize::fileExists(outfasta2) )
	{
		std::cerr << "[E] file " << outfasta2 << " already exists" << std::endl;
		return EXIT_FAILURE;
	}

	libmaus2::dazzler::db::DatabaseFile DB1(indb1);
	DB1.computeTrimVector();
	libmaus2::dazzler::db::DatabaseFile DB2(indb2);
	DB2.computeTrimVector();

	int64_t const tspace = libmaus2::dazzler::align::AlignmentFile::getTSpace(inlas);

	std::set<uint64_t> SA;
	std::set<uint64_t> SB;
	{
		libmaus2::dazzler::align::SimpleOverlapParser SOP(inlas);
		libmaus2::dazzler::align::SimpleOverlapParserGet SOPG(SOP);
		std::pair<uint8_t const *,uint8_t const *> P;

		while ( SOPG.getNext(P) )
		{
			libmaus2::dazzler::align::OverlapDataInterface ODI(P.first);
			SA.insert(ODI.aread());
			SB.insert(ODI.bread());
		}
	}
	std::vector<uint64_t> VA(SA.begin(),SA.end());
	std::vector<uint64_t> VB(SB.begin(),SB.end());

	{
		libmaus2::dazzler::align::SimpleOverlapParser SOP(inlas);
		libmaus2::dazzler::align::SimpleOverlapParserGet SOPG(SOP);
		std::pair<uint8_t const *,uint8_t const *> P;
		libmaus2::autoarray::AutoArray<uint8_t> A;

		std::cerr << "[V] writing alignments to " << outlas << std::endl;
		libmaus2::dazzler::align::AlignmentWriter AW(outlas,tspace,false /* index */);

		while ( SOPG.getNext(P) )
		{
			libmaus2::dazzler::align::OverlapDataInterface ODI(P.first);

			uint64_t const arank = std::lower_bound(VA.begin(),VA.end(),ODI.aread())-VA.begin();
			uint64_t const brank = std::lower_bound(VB.begin(),VB.end(),ODI.bread())-VB.begin();

			A.ensureSize(P.second-P.first);
			std::copy(P.first,P.second,A.begin());

			libmaus2::dazzler::align::OverlapData::putARead(A.begin(),arank);
			libmaus2::dazzler::align::OverlapData::putBRead(A.begin(),brank);

			AW.put(A.begin(),A.begin() + (P.second-P.first));
		}
	}

	{
		libmaus2::aio::OutputStreamInstance OSI(outfasta1);
		std::cerr << "[V] writing A reads to " << outfasta1 << std::endl;

		for ( uint64_t i = 0; i < VA.size(); ++i )
		{
			std::string const data = DB1[VA[i]];

			OSI << ">L0/" << VA[i] << "/" << 0 << "_" << data.size() << " RQ=0.851\n";
			char const * c = data.c_str();
			uint64_t rest = data.size();
			uint64_t const numcol = 80;

			while ( rest )
			{
				uint64_t const toprint = std::min(numcol,rest);
				OSI.write(c,toprint);
				OSI.put('\n');
				c += toprint;
				rest -= toprint;
			}
		}
	}

	{
		libmaus2::aio::OutputStreamInstance OSI(outfasta2);
		std::cerr << "[V] writing B reads to " << outfasta2 << std::endl;

		for ( uint64_t i = 0; i < VB.size(); ++i )
		{
			std::string const data = DB2[VB[i]];

			OSI << ">L1/" << VB[i] << "/" << 0 << "_" << data.size() << " RQ=0.851\n";
			char const * c = data.c_str();
			uint64_t rest = data.size();
			uint64_t const numcol = 80;

			while ( rest )
			{
				uint64_t const toprint = std::min(numcol,rest);
				OSI.write(c,toprint);
				OSI.put('\n');
				c += toprint;
				rest -= toprint;
			}
		}
	}

	return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser const arg(argc,argv);

		if ( arg.size() < 6 )
		{
			std::cerr << "usage: " << argv[0] << " <out.las> <out1.fasta> <out2.fasta> <A.db> <B.db> <in.las>\n";
			return EXIT_FAILURE;
		}

		int const r = lasextract(arg);

		return r;
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
