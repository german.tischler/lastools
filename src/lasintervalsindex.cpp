/*
    lasindex
    Copyright (C) 2015 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/dazzler/db/DatabaseFile.hpp>
#include <libmaus2/dazzler/align/LasIntervals.hpp>
#include <libmaus2/aio/SerialisedPeeker.hpp>
#include <config.h>

std::string getUsage(libmaus2::util::ArgParser const & arg)
{
	std::ostringstream ostr;

	ostr << "usage: " << arg.progname << " <in.db> <in.las> ..." << std::endl;

	return ostr.str();
}


int lasintervalsindex(libmaus2::util::ArgParser const & arg)
{
	if ( arg.uniqueArgPresent("printonly") )
	{
		libmaus2::dazzler::align::LasIntervals::unique_ptr_type tptr(
			libmaus2::dazzler::align::LasIntervals::load(arg[0])
		);

		std::cout << tptr->toString();

		return EXIT_SUCCESS;
	}
	if ( arg.uniqueArgPresent("concat") )
	{
		std::string const dbfn = arg[0];
		std::string const datafn = arg[1];

		libmaus2::dazzler::db::DatabaseFile const DB(arg[0]);
		uint64_t const n = DB.getTrimmedBlockSize(0);
		std::vector < libmaus2::dazzler::align::LasIntervals::LasIntervalsEntry > V;
		libmaus2::aio::SerialisedPeeker<libmaus2::dazzler::align::LasIntervals::LasIntervalsEntry> SP(datafn);
		libmaus2::dazzler::align::LasIntervals::LasIntervalsEntry E;

		while ( SP.getNext(E) )
			V.push_back(E);

		for ( uint64_t i = 0; i < V.size(); ++i )
			V[i].i = i;

		libmaus2::dazzler::align::LasIntervals LAI(V,n,std::cerr);
		LAI.serialise();

		std::cout << LAI.toString();

		return EXIT_SUCCESS;
	}

	libmaus2::dazzler::db::DatabaseFile const DB(arg[0]);
	// DB.computeTrimVector();

	bool const listmode = arg.uniqueArgPresent("l");

	std::vector<std::string> Vin;

	if ( listmode )
	{
		for ( uint64_t i = 1; i < arg.size(); ++i )
		{
			libmaus2::aio::InputStreamInstance ISI(arg[i]);
			std::string line;
			while ( std::getline(ISI,line) )
				Vin.push_back(line);
		}
	}
	else
	{
		for ( uint64_t i = 1; i < arg.size(); ++i )
			Vin.push_back(arg[i]);
	}

	libmaus2::dazzler::align::LasIntervals::createIndexIf(Vin,DB.getTrimmedBlockSize(0),std::cerr);

	libmaus2::dazzler::align::LasIntervals::unique_ptr_type tptr(
		libmaus2::dazzler::align::LasIntervals::load(Vin)
	);

	std::cout << tptr->toString();

	return EXIT_SUCCESS;
}

/**
 * create .idx and .bidx files for LAS/OVL files
 **/
int main(int argc, char *argv[])
{
	try
	{
		libmaus2::util::ArgParser const arg(argc,argv);

		if ( arg.argPresent("h") || arg.argPresent("help") )
		{
			std::cerr << getUsage(arg);
			return EXIT_SUCCESS;
		}
		else if ( arg.argPresent("version") )
		{
			std::cerr << "This is " << PACKAGE_NAME << " version " << PACKAGE_VERSION << std::endl;
			return EXIT_SUCCESS;
		}
		else if ( arg.size() < 2 )
		{
			std::cerr << getUsage(arg);
			return EXIT_FAILURE;
		}

		return lasintervalsindex(arg);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
