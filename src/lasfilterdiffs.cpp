/*
    lastools
    Copyright (C) 2015 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/dazzler/align/SimpleOverlapParser.hpp>

#include <libmaus2/dazzler/align/OverlapIndexer.hpp>
#include <libmaus2/dazzler/align/AlignmentWriter.hpp>
#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/dazzler/db/DatabaseFile.hpp>
#include <libmaus2/sorting/SortingBufferedOutputFile.hpp>
#include <libmaus2/dazzler/align/OverlapInfoIndexer.hpp>

#include <config.h>

std::string getUsage(libmaus2::util::ArgParser const & arg)
{
	std::ostringstream ostr;

	ostr << "usage: " << arg.progname << " -e<error_rate> <out.las> <db|dam> <in.las>" << std::endl;
	ostr << "\n";

	return ostr.str();
}

int lasfilterdiffs(libmaus2::util::ArgParser const & arg, libmaus2::util::ArgInfo const & /* arginfo */)
{
	if ( ! arg.uniqueArgPresent("e") )
	{
		std::cerr << "[E] mandatory argument -e not given" << std::endl;
		return EXIT_FAILURE;
	}

	double const e = arg.getParsedArg<double>("e");

	libmaus2::dazzler::align::SimpleOverlapParser::unique_ptr_type SOP(
        	new libmaus2::dazzler::align::SimpleOverlapParser(arg[2],256*1024)
	);
	int64_t const tspace = SOP->parser.tspace;
	libmaus2::dazzler::align::SimpleOverlapParserGet SOPG(*SOP);
	libmaus2::dazzler::db::DatabaseFile DB(arg[1]);
	DB.computeTrimVector();
	std::vector<uint64_t> RL;
	DB.getAllReadLengths(RL);
	std::pair<uint8_t const *,uint8_t const *> P;

	libmaus2::dazzler::align::AlignmentWriter AW(arg[0],tspace,true);
	std::string const symfn = arg[0] + ".sym";
	libmaus2::aio::OutputStreamInstance::unique_ptr_type OSI(
		new libmaus2::aio::OutputStreamInstance(symfn));

	while ( SOPG.getNext(P) )
	{
		libmaus2::dazzler::align::OverlapDataInterface ODI(P.first);

		int64_t const diffs = ODI.diffs();
		int64_t const aspan = ODI.aepos() - ODI.abpos();
		int64_t const bspan = ODI.bepos() - ODI.bbpos();
		int64_t const sspan = aspan + bspan;
		double const dsspan = sspan;
		double const erate = (2.0*diffs) / dsspan;

		if ( erate <= e )
		{
			AW.put(P.first,P.second);
		}
		else
		{
			ODI.getInfo().swappedStraight(RL.begin()).serialise(*OSI);
		}
	}

	OSI->flush();
	OSI.reset();

	libmaus2::sorting::SerialisingSortingBufferedOutputFile<libmaus2::dazzler::align::OverlapInfo>::sortUnique(
		symfn,1024*1024*1024ull /* blocksize */
	);
        libmaus2::dazzler::align::OverlapInfoIndexer::createInfoIndex(symfn,DB.size());

	return EXIT_SUCCESS;
}

/**
 *
 **/

int main(int argc, char *argv[])
{
	try
	{
		libmaus2::util::ArgInfo const arginfo(argc,argv);
		libmaus2::util::ArgParser const arg(argc,argv);

		if ( arg.argPresent("h") || arg.argPresent("help") )
		{
			std::cerr << getUsage(arg);
			return EXIT_SUCCESS;
		}
		else if ( arg.argPresent("version") )
		{
			std::cerr << "This is " << PACKAGE_NAME << " version " << PACKAGE_VERSION << std::endl;
			return EXIT_SUCCESS;
		}
		else if ( arg.size() < 3 )
		{
			std::cerr << getUsage(arg);
			return EXIT_FAILURE;
		}

		return lasfilterdiffs(arg,arginfo);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
