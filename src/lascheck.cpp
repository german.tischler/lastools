/*
    lastools
    Copyright (C) 2015 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/dazzler/align/SortingOverlapOutputBuffer.hpp>
#include <libmaus2/dazzler/db/DatabaseFile.hpp>
#include <libmaus2/parallel/NumCpus.hpp>
#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/random/Random.hpp>
#include <libmaus2/dazzler/align/SimpleOverlapParser.hpp>

#include <config.h>

std::string getUsage(libmaus2::util::ArgParser const & arg)
{
	std::ostringstream ostr;

	ostr << "usage: " << arg.progname << " <in_a.db> <in_b.db> <in.las> ..." << std::endl;
	ostr << "\n";
	ostr << "parameters:\n";

	return ostr.str();
}

int lascheck(libmaus2::util::ArgParser const & arg, libmaus2::util::ArgInfo const &)
{
	std::string const db0name = arg[0];
	std::string const db1name = arg[1];

	libmaus2::dazzler::db::DatabaseFile DB0(db0name);
	DB0.computeTrimVector();
	std::vector<uint64_t> RL0;
	DB0.getAllReadLengths(RL0);

	libmaus2::dazzler::db::DatabaseFile DB1(db1name);
	DB1.computeTrimVector();
	std::vector<uint64_t> RL1;
	DB1.getAllReadLengths(RL1);

	std::vector<std::string> Vin;
	for ( uint64_t i = 2; i < arg.size(); ++i )
		Vin.push_back(arg[i]);

	bool gok = true;
	for ( uint64_t z = 0; z < Vin.size(); ++z )
	{
		libmaus2::dazzler::align::SimpleOverlapParser SOP(Vin[z],16*1024*1024);
		libmaus2::autoarray::AutoArray<uint8_t> Aprev;
		uint64_t Aprevl = 0;

		libmaus2::autoarray::AutoArray<std::pair<uint16_t,uint16_t> > A;

		bool sortok = true;
		bool ok = true;
		std::ostringstream ostr;
		std::string const emptystr;

		while ( SOP.parseNextBlock() )
		{
			libmaus2::dazzler::align::OverlapData const & data = SOP.getData();
			int64_t const tspace = SOP.parser.tspace;

			for ( uint64_t i = 0; i < data.size(); ++i )
			{
				std::pair<uint8_t const *, uint8_t const *> const P = data.getData(i);
				std::pair<uint8_t const *, uint8_t const *> prevP;
				if ( i )
				{
					prevP = data.getData(i-1);
				}
				else
				{
					prevP.first = Aprev.begin();
					prevP.second = prevP.first + Aprevl;
				}

				if ( prevP.second != prevP.first )
					sortok = sortok && !libmaus2::dazzler::align::OverlapDataInterfaceFullComparator::compare(P.first,prevP.first);

				bool ostrused = false;
				libmaus2::dazzler::align::OverlapDataInterface const ODI(P.first);

				if ( ODI.abpos() < 0 )
				{
					ostr << "[E] broken abpos " << ODI.abpos() << std::endl;
					ostrused = true;
				}
				if ( ODI.aepos() > static_cast<int64_t>(RL0[ODI.aread()]) )
				{
					ostr << "[E] broken aepos " << ODI.aepos() << " > " << RL0[ODI.aread()] << std::endl;
					ostrused = true;
				}
				if ( ODI.bbpos() < 0 )
				{
					ostr << "[E] broken bbpos " << ODI.bbpos() << std::endl;
					ostrused = true;
				}
				if ( ODI.bepos() > static_cast<int64_t>(RL1[ODI.bread()]) )
				{
					ostr << "[E] broken bepos " << ODI.bepos() << " > " << RL1[ODI.bread()] << std::endl;
					ostrused = true;
				}

				uint64_t const tlen = ODI.decodeTraceVector(A, tspace);
				int64_t bspan = 0;
				for ( uint64_t j = 0; j < tlen; ++j )
					bspan += A[j].second;

				if ( bspan != ODI.bepos()-ODI.bbpos() )
				{
					ostr << "[E] bspan broken " << bspan << " != " << ODI.bepos()-ODI.bbpos() << std::endl;
					ostrused = true;
				}

				if ( ostrused )
				{
					std::cerr << ostr.str();

					ostr.str(emptystr);
					ostr.seekp(0);
					ostr.clear();

					ok = false;
				}
			}

			if ( data.size() )
			{
				std::pair<uint8_t const *, uint8_t const *> const P = data.getData(data.size()-1);
				uint64_t const l = P.second-P.first;
				if ( Aprev.size() < l )
					Aprev = libmaus2::autoarray::AutoArray<uint8_t>(l,false);

				Aprevl = l;
				std::copy(P.first,P.second,Aprev.begin());
			}
		}

		std::cout << Vin[z] << " " << (ok?"ok":"broken") << " sortorder " << (sortok?"ok":"unsorted") << std::endl;

		gok = gok && ok;
	}

	return gok ? EXIT_SUCCESS : EXIT_FAILURE;
}

/**
 * sort a set of LAS/OVL files and merge the sorted files to a single output file
 **/

int main(int argc, char *argv[])
{
	try
	{
		libmaus2::util::ArgInfo const arginfo(argc,argv);
		libmaus2::util::ArgParser const arg(argc,argv);

		if ( arg.argPresent("h") || arg.argPresent("help") )
		{
			std::cerr << getUsage(arg);
			return EXIT_SUCCESS;
		}
		else if ( arg.argPresent("version") )
		{
			std::cerr << "This is " << PACKAGE_NAME << " version " << PACKAGE_VERSION << std::endl;
			return EXIT_SUCCESS;
		}
		else if ( arg.size() < 2 )
		{
			std::cerr << getUsage(arg);
			return EXIT_FAILURE;
		}

		return lascheck(arg,arginfo);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
