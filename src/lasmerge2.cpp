/*
    lastools
    Copyright (C) 2015 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/dazzler/align/LasMerge2.hpp>
#include <libmaus2/dazzler/align/SortingOverlapOutputBuffer.hpp>
#include <libmaus2/parallel/NumCpus.hpp>
#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/util/GetFileSize.hpp>
#include <libmaus2/util/TempFileRemovalContainer.hpp>
#include <libmaus2/util/FiniteSizeHeap.hpp>
#include <libmaus2/dazzler/align/SimpleOverlapParser.hpp>

#include <config.h>

std::string getUsage(libmaus2::util::ArgParser const & arg)
{
	std::ostringstream ostr;

	ostr << "usage: " << arg.progname << " [-T<tmpprefix> -f<mergefanin> -l] <out.las> <in.las> ..." << std::endl;
	ostr << "\n";
	ostr << "parameters:\n";
	ostr << " -T : prefix for temporary files (default: create files in current working directory)\n";
	ostr << " -f : merge fan in (default: 64)\n";
	ostr << " -l : input files are not LAS but lists of LAS file names in text form (one file per line)\n";
	ostr << " --index : generate output file index\n";
	// ostr << " -s : sort order (canonical or ba, default: canonical)\n";

	return ostr.str();
}


int lasmerge2(libmaus2::util::ArgParser const & arg, libmaus2::util::ArgInfo const & arginfo)
{
	std::string const tmpfilebase = arg.uniqueArgPresent("T") ? arg["T"] : arginfo.getDefaultTmpFileName();
	uint64_t const mergefanin = arg.uniqueArgPresent("f") ? arg.getUnsignedNumericArg<uint64_t>("f") : 64;
	bool const lflag = arg.uniqueArgPresent("l");
	bool const index = arg.uniqueArgPresent("index");

	std::string const outfilename = arg[0];
	std::vector<std::string> infilenames;

	if ( lflag )
	{
		for ( uint64_t i = 1; i < arg.size(); ++i )
		{
			libmaus2::aio::InputStreamInstance ISI(arg[i]);
			std::string line;
			while ( ISI )
			{
				std::getline(ISI,line);
				if ( line.size() )
					infilenames.push_back(line);
			}
		}
	}
	else
	{
		for ( uint64_t i = 1; i < arg.size(); ++i )
			infilenames.push_back(arg[i]);
	}

	return libmaus2::dazzler::align::LasMerge2::lasmerge2(outfilename,infilenames,tmpfilebase,mergefanin,index);
}

/**
 * merge a set of sorted LAS/OVL files to a single sorted output file
 **/

int main(int argc, char *argv[])
{
	try
	{
		libmaus2::util::ArgInfo const arginfo(argc,argv);
		libmaus2::util::ArgParser const arg(argc,argv);

		if ( arg.argPresent("h") || arg.argPresent("help") )
		{
			std::cerr << getUsage(arg);
			return EXIT_SUCCESS;
		}
		else if ( arg.argPresent("version") )
		{
			std::cerr << "This is " << PACKAGE_NAME << " version " << PACKAGE_VERSION << std::endl;
			return EXIT_SUCCESS;
		}
		else if ( arg.size() < 2 )
		{
			std::cerr << getUsage(arg);
			return EXIT_FAILURE;
		}

		return lasmerge2(arg,arginfo);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
