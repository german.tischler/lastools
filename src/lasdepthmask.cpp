/*
    Copyright (C) 2018 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/util/GetFileSize.hpp>
#include <libmaus2/util/FiniteSizeHeap.hpp>
#include <libmaus2/dazzler/db/DatabaseFile.hpp>
#include <libmaus2/dazzler/align/SimpleOverlapParser.hpp>
#include <libmaus2/dazzler/align/OverlapDataInterface.hpp>
#include <libmaus2/dazzler/align/AlignmentWriter.hpp>
#include <libmaus2/sorting/SortingBufferedOutputFile.hpp>
#include <libmaus2/dazzler/align/OverlapInfoIndexer.hpp>

int lasdepthmask(libmaus2::util::ArgParser const & arg)
{
	std::string const outlas = arg[0];
	std::string const indb = arg[1];
	std::string const inlas = arg[2];

	if ( ! arg.uniqueArgPresent("d") )
	{
		std::cerr << "[E] required argument -d (depth) missing" << std::endl;
		return EXIT_FAILURE;
	}

	uint64_t const dthres = arg.getUnsignedNumericArg<uint64_t>("d");

	libmaus2::dazzler::db::DatabaseFile DB(indb);
	DB.computeTrimVector();

	std::vector<uint64_t> RL;
	DB.getAllReadLengths(RL);

	// int64_t const tspace = libmaus2::dazzler::align::AlignmentFile::getTSpace(inlas);

	libmaus2::dazzler::align::SimpleOverlapParser::unique_ptr_type SOP(new libmaus2::dazzler::align::SimpleOverlapParser(inlas));
	int64_t const tspace = SOP->AF.tspace;
	libmaus2::dazzler::align::SimpleOverlapParserGet::unique_ptr_type SOPG(new libmaus2::dazzler::align::SimpleOverlapParserGet(*SOP));
	std::pair<uint8_t const *,uint8_t const *> P;

	struct Comp
	{
		bool operator()(std::pair<uint64_t,uint64_t> const & A, std::pair<uint64_t,uint64_t> const & B) const
		{
			if ( A.first != B.first )
				return A.first < B.first;
			else
				return A.second > B.second;
		}
	};

	struct RepeatEntry
	{
		uint64_t aread;
		uint64_t from;
		uint64_t to;

		RepeatEntry() {}
		RepeatEntry(uint64_t const raread, uint64_t const rfrom, uint64_t const rto)
		: aread(raread), from(rfrom), to(rto) {}
	};

	std::vector<RepeatEntry> VRE;

	while ( SOPG->peekNext(P) )
	{
		libmaus2::dazzler::align::OverlapDataInterface RODI(P.first);

		int64_t const aread = RODI.aread();
		uint64_t const rl = RL[aread]; // DB.getRead(aread).rlen;

		std::vector< std::pair<uint64_t,uint64_t> > VP;

		while ( SOPG->peekNext(P) && libmaus2::dazzler::align::OverlapDataInterface(P.first).aread() == aread )
		{
			libmaus2::dazzler::align::OverlapDataInterface ODI(P.first);
			VP.push_back(std::pair<uint64_t,uint64_t>(ODI.abpos(),ODI.aepos()));
			SOPG->getNext(P);
		}

		std::sort(VP.begin(),VP.end(),Comp());

		libmaus2::util::FiniteSizeHeap < std::pair<uint64_t,uint64_t> > H(1024);

		uint64_t d = 0;
		uint64_t prevstart = 0;

		typedef libmaus2::math::IntegerInterval<int64_t> itype;
		std::vector<itype> VI;

		uint64_t dstart = 0;
		for ( uint64_t i = 0; i < VP.size(); ++i )
		{
			while ( ! H.empty() && H.top().first <= VP[i].first )
			{
				uint64_t const e = H.top().first;

				#if 0
				if ( e > prevstart )
					std::cout << aread << ":" << "[" << prevstart << "," << e << ")\t" << d << std::endl;
				#endif

				if ( d == dthres )
				{
					// std::cout << aread << ":" << "[" << dstart << "," << e << ")/" << rl << "\t" << d << std::endl;

					int64_t start = (dstart / tspace)*tspace;
					int64_t end = std::min ( ((e + tspace - 1)/tspace) * tspace, rl );

					VI.push_back(itype(start,end-1));
				}

				H.pop();
				d -= 1;
				prevstart = e;
			}

			if ( VP[i].first > prevstart )
			{
				#if 0
				std::cout << aread << ":" << "[" << prevstart << "," << VP[i].first << ")\t" << d << std::endl;
				#endif
				prevstart = VP[i].first;
			}

			H.pushBump(std::pair<uint64_t,uint64_t>(VP[i].second,i));
			d += 1;

			if ( d == dthres )
				dstart = VP[i].first;
		}

		while ( ! H.empty() )
		{
			uint64_t const e = H.top().first;

			#if 0
			if ( e > prevstart )
				std::cout << aread << ":" << "[" << prevstart << "," << e << ")\t" << d << std::endl;
			#endif

			if ( d == dthres )
			{
				// std::cout << aread << ":" << "[" << dstart << "," << e << ")/" << rl << "\t" << d << std::endl;
				int64_t start = (dstart / tspace)*tspace;
				int64_t end = std::min ( ((e + tspace - 1)/tspace) * tspace, rl );
				VI.push_back(itype(start,end-1));
			}

			H.pop();
			d -= 1;
			prevstart = e;
		}

		VI = itype::mergeTouchingOrOverlapping(VI);

		for ( uint64_t i = 0; i < VI.size(); ++i )
		{
			std::cerr << "[V] mask region " << aread << ":" << VI[i] << "/" << rl << "\n";
			VRE.push_back(
				RepeatEntry(aread,VI[i].from,VI[i].from + VI[i].diameter())
			);
		}

		#if 0
		if ( prevstart != rl )
		{
			std::cout << aread << ":" << "[" << prevstart << "," << rl << ")\t" << d << std::endl;
		}
		#endif
	}

	SOPG.reset();
	SOP.reset();

	{
		libmaus2::dazzler::align::SimpleOverlapParser::unique_ptr_type tSOP(new libmaus2::dazzler::align::SimpleOverlapParser(inlas));
		SOP = std::move(tSOP);
		libmaus2::dazzler::align::SimpleOverlapParserGet::unique_ptr_type tSOPG(new libmaus2::dazzler::align::SimpleOverlapParserGet(*SOP));
		SOPG = std::move(tSOPG);
	}

	std::string const pairfn = outlas + ".pairs";
	libmaus2::util::TempFileRemovalContainer::addTempFile(pairfn);
	libmaus2::aio::OutputStreamInstance::unique_ptr_type Ppairs(new libmaus2::aio::OutputStreamInstance(pairfn));

	uint64_t VRElow = 0;

	while ( SOPG->peekNext(P) )
	{
		libmaus2::dazzler::align::OverlapDataInterface RODI(P.first);

		int64_t const aread = RODI.aread();
		// uint64_t const rl = DB.getRead(aread).rlen;

		while ( VRElow < VRE.size() && static_cast<int64_t>(VRE[VRElow].aread) < aread )
			++VRElow;

		uint64_t VREhigh = VRElow;
		while ( VREhigh < VRE.size() && static_cast<int64_t>(VRE[VREhigh].aread) == aread )
			++VREhigh;

		std::cerr << "[V] getting pairs for " << aread << " " << VREhigh-VRElow << std::endl;

		if ( VREhigh - VRElow )
		{
			libmaus2::util::NumberSerialisation::serialiseNumber(*Ppairs,aread);

			uint64_t prevbread = std::numeric_limits<uint64_t>::max();

			while ( SOPG->peekNext(P) && libmaus2::dazzler::align::OverlapDataInterface(P.first).aread() == aread )
			{
				libmaus2::dazzler::align::OverlapDataInterface ODI(P.first);
				uint64_t const abpos = ODI.abpos();
				uint64_t const aepos = ODI.aepos();
				uint64_t const bread = ODI.bread();
				bool keep = true;
				for ( uint64_t i = VRElow; i < VREhigh; ++i )
					if ( abpos >= VRE[i].from && aepos <= VRE[i].to )
						keep = false;

				SOPG->getNext(P);

				if ( keep && bread != prevbread )
				{
					libmaus2::util::NumberSerialisation::serialiseNumber(*Ppairs,bread);
				}

				prevbread = bread;
			}

			libmaus2::util::NumberSerialisation::serialiseNumber(*Ppairs,std::numeric_limits<uint64_t>::max());
		}
		else
		{
			while ( SOPG->peekNext(P) && libmaus2::dazzler::align::OverlapDataInterface(P.first).aread() == aread )
				SOPG->getNext(P);
		}

		VRElow = VREhigh;
	}

	Ppairs->flush();
	Ppairs.reset();

	SOPG.reset();
	SOP.reset();

	{
		libmaus2::dazzler::align::SimpleOverlapParser::unique_ptr_type tSOP(new libmaus2::dazzler::align::SimpleOverlapParser(inlas));
		SOP = std::move(tSOP);
		libmaus2::dazzler::align::SimpleOverlapParserGet::unique_ptr_type tSOPG(new libmaus2::dazzler::align::SimpleOverlapParserGet(*SOP));
		SOPG = std::move(tSOPG);
	}

	VRElow = 0;

	libmaus2::dazzler::align::AlignmentWriter AW(outlas,tspace);
	std::string const symfn = outlas + ".sym";
	libmaus2::aio::OutputStreamInstance::unique_ptr_type Psym(new libmaus2::aio::OutputStreamInstance(symfn));
	libmaus2::aio::InputStreamInstance::unique_ptr_type Ppairsin(new libmaus2::aio::InputStreamInstance(pairfn));

	uint64_t nremove = 0;
	uint64_t nkeep = 0;

	while ( SOPG->peekNext(P) )
	{
		libmaus2::dazzler::align::OverlapDataInterface RODI(P.first);

		int64_t const aread = RODI.aread();
		// uint64_t const rl = DB.getRead(aread).rlen;

		while ( VRElow < VRE.size() && static_cast<int64_t>(VRE[VRElow].aread) < aread )
			++VRElow;

		uint64_t VREhigh = VRElow;
		while ( VREhigh < VRE.size() && static_cast<int64_t>(VRE[VREhigh].aread) == aread )
			++VREhigh;

		std::cerr << "[V] filtering " << aread << " " << VREhigh-VRElow << std::endl;

		if ( VREhigh - VRElow )
		{
			int64_t ch_aread = libmaus2::util::NumberSerialisation::deserialiseNumber(*Ppairsin);
			assert ( ch_aread == aread );

			uint64_t vb;

			std::set<uint64_t> SB;
			while ( (vb = libmaus2::util::NumberSerialisation::deserialiseNumber(*Ppairsin)) != std::numeric_limits<uint64_t>::max() )
				SB.insert(vb);

			assert ( vb == std::numeric_limits<uint64_t>::max() );

			while ( SOPG->peekNext(P) && libmaus2::dazzler::align::OverlapDataInterface(P.first).aread() == aread )
			{
				libmaus2::dazzler::align::OverlapDataInterface ODI(P.first);
				uint64_t const bread = ODI.bread();
				bool keep = SB.find(bread) != SB.end();

				SOPG->getNext(P);

				if ( keep )
				{
					AW.put(P.first,P.second);
					nkeep++;
				}
				else
				{
					ODI.getInfo().swappedStraight(RL.begin()).serialise(*Psym);
					nremove++;
					// std::cerr << "removing " << ODI << std::endl;
				}
			}
		}
		else
		{
			while ( SOPG->peekNext(P) && libmaus2::dazzler::align::OverlapDataInterface(P.first).aread() == aread )
			{
				SOPG->getNext(P);
				AW.put(P.first,P.second);
				nkeep++;
			}
		}

		VRElow = VREhigh;
	}

	Psym->flush();
	Psym.reset();

        libmaus2::sorting::SerialisingSortingBufferedOutputFile<libmaus2::dazzler::align::OverlapInfo>::sort(symfn,16*1024*1024);
        libmaus2::dazzler::align::OverlapInfoIndexer::createInfoIndex(symfn,DB.size());

        std::cerr << "[V] kept " << nkeep << " removed " << nremove << std::endl;

	return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser const arg(argc,argv);

		if ( arg.size() < 3 )
		{
			std::cerr << "usage: " << argv[0] << " -d<depth> <out.las> <in.db> <in.las>\n";
			return EXIT_FAILURE;
		}

		int const r = lasdepthmask(arg);

		return r;
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
