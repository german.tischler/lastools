/*
    lastools
    Copyright (C) 2015 German Tischler

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/dazzler/align/SortingOverlapOutputBuffer.hpp>
#include <libmaus2/parallel/NumCpus.hpp>
#include <libmaus2/util/ArgInfo.hpp>
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/util/GetFileSize.hpp>
#include <libmaus2/util/TempFileRemovalContainer.hpp>
#include <libmaus2/util/FiniteSizeHeap.hpp>
#include <libmaus2/dazzler/align/SimpleOverlapParser.hpp>
#include <libmaus2/dazzler/align/OverlapIndexer.hpp>
#include <libmaus2/dazzler/db/DatabaseFile.hpp>

#include <config.h>

std::string getUsage(libmaus2::util::ArgParser const & arg)
{
	std::ostringstream ostr;

	ostr << "usage: " << arg.progname << " [-T<tmpprefix> -f<mergefanin> -l -b<blockid>] <out.las> <in.db> <in.las> ..." << std::endl;
	ostr << "\n";
	ostr << "parameters:\n";
	ostr << " -T : prefix for temporary files (default: create files in current working directory)\n";
	ostr << " -f : merge fan in (default: 64)\n";
	ostr << " -J : range selector (no default)\n";
	ostr << " -l : input files are not LAS but lists of LAS file names in text form (one file per line)\n";
	ostr << " --index : generate output file index\n";
	// ostr << " -s : sort order (canonical or ba, default: canonical)\n";

	return ostr.str();
}

void runMerge(std::string const & O, std::vector<std::string> const & I, uint64_t const from, uint64_t const to)
{
	int64_t const tspace = I.size() ? libmaus2::dazzler::align::AlignmentFile::getTSpace(I[0]) : 0;
	libmaus2::dazzler::align::AlignmentWriter::unique_ptr_type AW(new libmaus2::dazzler::align::AlignmentWriter(O, tspace, true /* index */, 0 /* novl */));

	libmaus2::autoarray::AutoArray < libmaus2::aio::InputStreamInstance::unique_ptr_type > AISI(I.size());
	libmaus2::autoarray::AutoArray < libmaus2::dazzler::align::SimpleOverlapParser::unique_ptr_type > ASOP(I.size());
	libmaus2::util::FiniteSizeHeap < libmaus2::dazzler::align::OverlapData::DataIndex, libmaus2::dazzler::align::OverlapDataInterfaceFullComparator > FSH(I.size());

	for ( uint64_t i = 0; i < I.size(); ++i )
	{
		libmaus2::dazzler::align::OverlapIndexer::constructIndexIf(I[i]);
		std::string const dalindexname = libmaus2::dazzler::align::DalignerIndexDecoder::getDalignerIndexName(I[i]);

		libmaus2::dazzler::align::DalignerIndexDecoder::unique_ptr_type tdalindex(
			new libmaus2::dazzler::align::DalignerIndexDecoder(I[i],dalindexname)
		);
		uint64_t const filefrom = (*tdalindex)[from];
		uint64_t const fileto = (*tdalindex)[to];

		tdalindex.reset();

		libmaus2::aio::InputStreamInstance::unique_ptr_type tISI(
			new libmaus2::aio::InputStreamInstance(I[i])
		);
		AISI[i] = std::move(tISI);
		AISI[i]->clear();
		AISI[i]->seekg(filefrom);

		libmaus2::dazzler::align::SimpleOverlapParser::unique_ptr_type SOP(
			new libmaus2::dazzler::align::SimpleOverlapParser(
				*(AISI[i]),
				tspace,
				256*1024 /* buf size */,
				libmaus2::dazzler::align::OverlapParser::overlapparser_do_split,
				fileto-filefrom /* limit */
			)
		);

		ASOP[i] = std::move(SOP);

		while ( ASOP[i]->parseNextBlock() )
		{
			if ( ASOP[i]->getData().size() )
			{
				FSH.push(libmaus2::dazzler::align::OverlapData::DataIndex(
					i,&(ASOP[i]->getData()),0)
				);

				break;
			}
		}
	}

	while ( ! FSH.empty() )
	{
		libmaus2::dazzler::align::OverlapData::DataIndex D = FSH.pop();

		std::pair<uint8_t const *, uint8_t const *> const P = D.data->getData(D.index);

		AW->put(P.first,P.second);

		if ( D.index + 1 < D.data->size() )
		{
			D.index += 1;
			FSH.push(D);
		}
		else
		{
			while ( ASOP[D.parserid]->parseNextBlock() )
				if ( ASOP[D.parserid]->getData().size() )
				{
					D.index = 0;
					FSH.push(D);
					break;
				}
		}
	}
}

template<typename comparator_type>
int lasmerge2Template(libmaus2::util::ArgParser const & arg, libmaus2::util::ArgInfo const & arginfo)
{
	comparator_type comp;
	std::string const tmpfilebase = arg.uniqueArgPresent("T") ? arg["T"] : arginfo.getDefaultTmpFileName();
	uint64_t const mergefanin = arg.uniqueArgPresent("f") ? arg.getUnsignedNumericArg<uint64_t>("f") : 64;
	bool const lflag = arg.uniqueArgPresent("l");
	uint64_t nextid = 0;

	int const Jflag = arg.uniqueArgPresent("J");

	if ( Jflag != 1 )
	{
		std::cerr << "[E] need J flag" << std::endl;
		return EXIT_FAILURE;
	}

	std::string const outfilename = arg[0];
	std::string const dbfn = arg[1];
	std::vector<std::string> infilenames;

	if ( lflag )
	{
		for ( uint64_t i = 2; i < arg.size(); ++i )
		{
			libmaus2::aio::InputStreamInstance ISI(arg[i]);
			std::string line;
			while ( ISI )
			{
				std::getline(ISI,line);
				if ( line.size() )
					infilenames.push_back(line);
			}
		}
	}
	else
	{
		for ( uint64_t i = 2; i < arg.size(); ++i )
			infilenames.push_back(arg[i]);
	}

	assert ( infilenames.size() );

	libmaus2::dazzler::db::DatabaseFile const DB(dbfn);

	assert ( Jflag );

	int64_t imin = 0, itop = 0;

	std::string const Js = arg["J"];
	std::istringstream istr(Js);
	int64_t Icnt;
	istr >> Icnt;

	if ( ! istr )
	{
		libmaus2::exception::LibMausException lme;
		lme.getStream() << "[E] unable to parse " << Js << std::endl;
		lme.finish();
		throw lme;
	}

	int const c = istr.get();

	if ( ! istr || c == std::istream::traits_type::eof() || c != ',' )
	{
		libmaus2::exception::LibMausException lme;
		lme.getStream() << "[E] unable to parse " << Js << std::endl;
		lme.finish();
		throw lme;
	}

	int64_t Idiv;
	istr >> Idiv;

	if ( ! istr || istr.peek() != std::istream::traits_type::eof() )
	{
		libmaus2::exception::LibMausException lme;
		lme.getStream() << "[E] unable to parse " << Js << std::endl;
		lme.finish();
		throw lme;
	}

	int64_t const top = DB.getTrimmedBlockSize(0);
	int64_t const span = top;

	if ( span && ! Idiv )
	{
		libmaus2::exception::LibMausException lme;
		lme.getStream() << "[E] denominator of J argument cannot be zero" << std::endl;
		lme.finish();
		throw lme;
	}

	if ( span )
	{
		int64_t const partsize = Idiv ? (span + Idiv - 1)/Idiv : 0;

		imin = std::min(Icnt * partsize,top);
		itop = std::min(imin+partsize,top);
	}

	assert ( mergefanin > 1 );
	bool deleteinput = false;

	while ( infilenames.size() > mergefanin )
	{
		uint64_t const packs = ( infilenames.size() + mergefanin - 1 ) / mergefanin;
		uint64_t const tmergefanin = ( infilenames.size() + packs - 1 ) / packs;

		uint64_t s = 0;
		std::vector<std::string> IV(packs);
		for ( uint64_t z = 0; z < packs; ++z )
		{
			uint64_t const low  = z * tmergefanin;
			uint64_t const high = std::min(low+tmergefanin,static_cast<uint64_t>(infilenames.size()));
			assert ( high > low );
			s += high - low;

			std::vector<std::string> I(infilenames.begin()+low,infilenames.begin()+high);
			std::ostringstream fnostr;
			fnostr << tmpfilebase << "_" << std::setw(6) << std::setfill('0') << (nextid++);
			std::string const O = fnostr.str();
			libmaus2::util::TempFileRemovalContainer::addTempFile(O);

			runMerge(O,I,imin,itop);

			if ( deleteinput )
				for ( uint64_t i = 0; i < I.size(); ++i )
					libmaus2::dazzler::align::SortingOverlapOutputBuffer<>::removeFileAndIndex(I[i]);

			IV[z] = O;
		}
		assert ( s == infilenames.size() );

		infilenames = IV;
		deleteinput = true;
	}

	assert ( infilenames.size() <= mergefanin );

	runMerge(outfilename,infilenames,imin,itop);

	std::cout << outfilename << std::endl;

	if ( deleteinput )
		for ( uint64_t i = 0; i < infilenames.size(); ++i )
			libmaus2::dazzler::align::SortingOverlapOutputBuffer<>::removeFileAndIndex(infilenames[i]);

	return EXIT_SUCCESS;
}

int lasmerge2(libmaus2::util::ArgParser const & arg, libmaus2::util::ArgInfo const & arginfo)
{
	// std::string const sortorder = arg.uniqueArgPresent("s") ? arg["s"] : std::string("full");
	return lasmerge2Template<libmaus2::dazzler::align::OverlapDataInterfaceFullComparator>(arg,arginfo);
}

/**
 * merge a set of sorted LAS/OVL files to a single sorted output file
 **/

int main(int argc, char *argv[])
{
	try
	{
		libmaus2::util::ArgInfo const arginfo(argc,argv);
		libmaus2::util::ArgParser const arg(argc,argv);

		if ( arg.argPresent("h") || arg.argPresent("help") )
		{
			std::cerr << getUsage(arg);
			return EXIT_SUCCESS;
		}
		else if ( arg.argPresent("version") )
		{
			std::cerr << "This is " << PACKAGE_NAME << " version " << PACKAGE_VERSION << std::endl;
			return EXIT_SUCCESS;
		}
		else if ( arg.size() < 2 )
		{
			std::cerr << getUsage(arg);
			return EXIT_FAILURE;
		}

		return lasmerge2(arg,arginfo);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
