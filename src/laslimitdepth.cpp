/*
    lastools
    Copyright (C) 2019 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/dazzler/db/DatabaseFile.hpp>
#include <libmaus2/dazzler/align/AlignmentWriter.hpp>
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/dazzler/align/SimpleOverlapParser.hpp>
#include <libmaus2/dazzler/align/OverlapDataInterface.hpp>

#include <config.h>

std::string getUsage(libmaus2::util::ArgParser const & arg)
{
	std::ostringstream ostr;

	ostr << "usage: " << arg.progname << " -d<depth> <out.las> <in.db> <in.las> ..." << std::endl;
	ostr << "\n";
	ostr << "parameters:\n";

	return ostr.str();
}

int laslimitdepth(libmaus2::util::ArgParser const & arg)
{
	std::string const outfilename = arg[0];
	std::string const dbfn = arg[1];

	if ( ! arg.uniqueArgPresent("d") )
	{
		std::cerr << "[E] required argument -d missing" << std::endl;
		return EXIT_FAILURE;
	}

	bool const verbose = arg.uniqueArgPresent("v");

	libmaus2::dazzler::db::DatabaseFile DB(dbfn);
	DB.computeTrimVector();
	std::vector<uint64_t> RL;
	DB.getAllReadLengths(RL);

	uint64_t const d = arg.getUnsignedNumericArg<uint64_t>("d");

	std::vector<std::string> VI;
	for ( uint64_t i = 2; i < arg.size(); ++i )
		VI.push_back(arg[i]);
	int64_t const tspace = libmaus2::dazzler::align::AlignmentFile::getTSpace(VI);

	libmaus2::dazzler::align::AlignmentWriter::unique_ptr_type AW(
		new libmaus2::dazzler::align::AlignmentWriter(outfilename,tspace,false /* index */, 0 /* expt */)
	);

	libmaus2::dazzler::align::SimpleOverlapParserConcat SOPC(VI,1024*1024);
	libmaus2::dazzler::align::SimpleOverlapParserConcatGet SOPCG(SOPC);
	std::pair<uint8_t const *, uint8_t const *> P;

	int64_t prevaread = -1;
	int64_t prevapos = -1;
	std::vector<uint64_t> VD;

	while ( SOPCG.getNext(P) )
	{
		libmaus2::dazzler::align::OverlapDataInterface const ODI(P.first);
		int64_t const abpos = ODI.abpos();
		int64_t const aepos = ODI.aepos();
		int64_t const aread = ODI.aread();

		if ( aread != prevaread )
		{
			if ( aread < prevaread )
			{
				std::cerr << "[E] file is not sorted by a-read" << std::endl;
				return EXIT_FAILURE;
			}

			prevaread = aread;
			prevapos = abpos;

			std::cerr << "[V] switching to A-read " << aread << " of length " << RL[aread] << std::endl;

			VD.resize(RL[aread]);
			std::fill(VD.begin(),VD.end(),0ull);
		}

		if ( abpos < prevapos )
		{
			std::cerr << "[E] file is not sorted by abpos" << std::endl;
			return EXIT_FAILURE;
		}

		bool keep = false;

		for ( int64_t i = abpos; (!keep) && i < aepos; ++i )
			if ( ++VD[i] <= d )
			{
				keep = true;

				if ( verbose )
					std::cerr << "[V] keeping " << ODI << " for position " << i << std::endl;
			}

		if ( keep )
			AW->put(P.first,P.second);
		else if ( verbose )
			std::cerr << "[V] discarding " << ODI << std::endl;

		prevapos = abpos;
	}

	AW.reset();

	return EXIT_SUCCESS;
}

/**
 * sort a set of LAS/OVL files and merge the sorted files to a single output file
 **/

int main(int argc, char *argv[])
{
	try
	{
		libmaus2::util::ArgParser const arg(argc,argv);

		if ( arg.argPresent("h") || arg.argPresent("help") )
		{
			std::cerr << getUsage(arg);
			return EXIT_SUCCESS;
		}
		else if ( arg.argPresent("version") )
		{
			std::cerr << "This is " << PACKAGE_NAME << " version " << PACKAGE_VERSION << std::endl;
			return EXIT_SUCCESS;
		}
		else if ( arg.size() < 1 )
		{
			std::cerr << getUsage(arg);
			return EXIT_FAILURE;
		}

		return laslimitdepth(arg);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
