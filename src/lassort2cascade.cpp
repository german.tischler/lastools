/*
    lassort2
    Copyright (C) 2018 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/dazzler/align/LasSort2Cascade.hpp>
#include <config.h>

std::string getUsage(libmaus2::util::ArgParser const & arg)
{
	std::ostringstream ostr;

	ostr << "usage: " << arg.progname << " [-M<memory> -l -t<numthreads> -T<tmpprefix> -f<mergefanin> -s<sortorder> --index --deletein --setdep -D<depfile>] <out.las> <in.las> ..." << std::endl;
	ostr << "\n";
	ostr << "parameters:\n";
	ostr << " -t         : number of threads (defaults to number of cores on machine)\n";
	ostr << " -T         : prefix for temporary files (default: create files in current working directory)\n";
	ostr << " -f         : merge fan in (default: " << libmaus2::dazzler::align::LasSort2Cascade::getDefaultFanIn() << ")\n";
	ostr << " -s         : sort order\n";
	ostr << " -l         : list mode (input contains list of file names instead of actual LAS files)\n";
	ostr << " -M         : memory block size\n";
	ostr << " --index    : construct index for output file\n";
	ostr << " --deletein : delete input files after sorting\n";
	ostr << " --setdep   : set input files as dependencies for first merge level\n";
	ostr << " -D         : file containing dependencies for first merge level\n";

	return ostr.str();
}


int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser const arg(argc,argv);

		if ( arg.argPresent("h") || arg.argPresent("help") )
		{
			std::cerr << getUsage(arg);
			return EXIT_SUCCESS;
		}
		else if ( arg.argPresent("version") )
		{
			std::cerr << "This is " << PACKAGE_NAME << " version " << PACKAGE_VERSION << std::endl;
			return EXIT_SUCCESS;
		}
		else if ( arg.size() < 2 )
		{
			std::cerr << getUsage(arg);
			return EXIT_FAILURE;
		}

		libmaus2::dazzler::align::LasSort2Cascade::lassort2cascade(arg);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
