/*
    Copyright (C) 2018 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/util/GetFileSize.hpp>
#include <libmaus2/util/FiniteSizeHeap.hpp>
#include <libmaus2/dazzler/db/DatabaseFile.hpp>
#include <libmaus2/dazzler/align/SimpleOverlapParser.hpp>
#include <libmaus2/dazzler/align/OverlapDataInterface.hpp>
#include <libmaus2/dazzler/align/AlignmentWriter.hpp>

int lasreaddepth(libmaus2::util::ArgParser const & arg)
{
	std::string const indb = arg[0];
	std::string const inlas = arg[1];

	libmaus2::dazzler::db::DatabaseFile DB(indb);
	DB.computeTrimVector();

	// int64_t const tspace = libmaus2::dazzler::align::AlignmentFile::getTSpace(inlas);

	libmaus2::dazzler::align::SimpleOverlapParser SOP(inlas);
	libmaus2::dazzler::align::SimpleOverlapParserGet SOPG(SOP);
	std::pair<uint8_t const *,uint8_t const *> P;

	struct Comp
	{
		bool operator()(std::pair<uint64_t,uint64_t> const & A, std::pair<uint64_t,uint64_t> const & B) const
		{
			if ( A.first != B.first )
				return A.first < B.first;
			else
				return A.second > B.second;
		}
	};

	while ( SOPG.peekNext(P) )
	{
		libmaus2::dazzler::align::OverlapDataInterface RODI(P.first);

		int64_t const aread = RODI.aread();
		uint64_t const rl = DB.getRead(aread).rlen;

		std::vector< std::pair<uint64_t,uint64_t> > VP;

		while ( SOPG.peekNext(P) && libmaus2::dazzler::align::OverlapDataInterface(P.first).aread() == aread )
		{
			libmaus2::dazzler::align::OverlapDataInterface ODI(P.first);
			VP.push_back(std::pair<uint64_t,uint64_t>(ODI.abpos(),ODI.aepos()));
			SOPG.getNext(P);
		}

		std::sort(VP.begin(),VP.end(),Comp());

		libmaus2::util::FiniteSizeHeap < std::pair<uint64_t,uint64_t> > H(1024);

		uint64_t d = 0;
		uint64_t prevstart = 0;
		for ( uint64_t i = 0; i < VP.size(); ++i )
		{
			while ( ! H.empty() && H.top().first <= VP[i].first )
			{
				uint64_t const e = H.top().first;

				if ( e > prevstart )
					std::cout << aread << ":" << "[" << prevstart << "," << e << ")\t" << d << std::endl;

				H.pop();
				d -= 1;
				prevstart = e;
			}

			if ( VP[i].first > prevstart )
			{
				std::cout << aread << ":" << "[" << prevstart << "," << VP[i].first << ")\t" << d << std::endl;
				prevstart = VP[i].first;
			}

			H.pushBump(std::pair<uint64_t,uint64_t>(VP[i].second,i));
			d += 1;
		}

		while ( ! H.empty() )
		{
			uint64_t const e = H.top().first;

			if ( e > prevstart )
				std::cout << aread << ":" << "[" << prevstart << "," << e << ")\t" << d << std::endl;

			H.pop();
			d -= 1;
			prevstart = e;
		}

		if ( prevstart != rl )
		{
			std::cout << aread << ":" << "[" << prevstart << "," << rl << ")\t" << d << std::endl;
		}
	}

	return EXIT_SUCCESS;
}

int main(int argc, char * argv[])
{
	try
	{
		libmaus2::util::ArgParser const arg(argc,argv);

		if ( arg.size() < 2 )
		{
			std::cerr << "usage: " << argv[0] << " <in.db> <in.las>\n";
			return EXIT_FAILURE;
		}

		int const r = lasreaddepth(arg);

		return r;
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
