/*
    lastools
    Copyright (C) 2018 German Tischler-Höhle

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <libmaus2/util/ArgParser.hpp>
#include <libmaus2/dazzler/align/OverlapIndexer.hpp>
#include <libmaus2/dazzler/align/LasIntervals.hpp>
#include <libmaus2/parallel/NumCpus.hpp>
#include <config.h>

std::string getUsage(libmaus2::util::ArgParser const & arg)
{
	std::ostringstream ostr;

	ostr << "usage: " << arg.progname << " <in.las> ..." << std::endl;

	return ostr.str();
}


int lasindexentry(libmaus2::util::ArgParser const & arg)
{
	std::vector<std::string> Vlas;

	bool const listmode = arg.uniqueArgPresent("l");

	if ( listmode )
	{
		for ( uint64_t z = 0; z < arg.size(); ++z )
		{
			libmaus2::aio::InputStreamInstance ISI(arg[z]);

			while ( ISI )
			{
				std::string s;
				ISI >> s;

				if ( s.size() )
					Vlas.push_back(s);
			}
		}

		for ( uint64_t i = 0; i < Vlas.size(); ++i )
			std::cerr << "[L]\t" << Vlas[i] << std::endl;
	}
	else
	{
		for ( uint64_t z = 0; z < arg.size(); ++z )
			Vlas.push_back(arg[z]);
	}

	uint64_t const numthreads = arg.uniqueArgPresent("t") ? arg.getUnsignedNumericArg<uint64_t>("t") : libmaus2::parallel::NumCpus::getNumLogicalProcessors();
	std::vector<std::string> V(Vlas.size());

	#if defined(_OPENMP)
	#pragma omp parallel for num_threads(numthreads) schedule(dynamic,1)
	#endif
	for ( uint64_t a = 0; a < Vlas.size(); ++a )
	{
		std::ostringstream ostr;
		libmaus2::dazzler::align::LasIntervals::getEntry(Vlas[a],a).serialise(ostr);
		V[a] = ostr.str();
	}

	for ( uint64_t i = 0; i < V.size(); ++i )
	{
		std::string const & s = V[i];
		std::cout.write(s.c_str(),s.size());
	}

	return EXIT_SUCCESS;
}

/**
 * create .idx and .bidx files for LAS/OVL files
 **/
int main(int argc, char *argv[])
{
	try
	{
		libmaus2::util::ArgParser const arg(argc,argv);

		if ( arg.argPresent("h") || arg.argPresent("help") )
		{
			std::cerr << getUsage(arg);
			return EXIT_SUCCESS;
		}
		else if ( arg.argPresent("version") )
		{
			std::cerr << "This is " << PACKAGE_NAME << " version " << PACKAGE_VERSION << std::endl;
			return EXIT_SUCCESS;
		}
		else if ( arg.size() < 1 )
		{
			std::cerr << getUsage(arg);
			return EXIT_FAILURE;
		}

		return lasindexentry(arg);
	}
	catch(std::exception const & ex)
	{
		std::cerr << ex.what() << std::endl;
		return EXIT_FAILURE;
	}
}
